name := "tester"

version := "0.2"

libraryDependencies ++= Seq(
  "com.github.jsuereth.scala-arm" %% "scala-arm" % "1.0",
  "com.yammer.metrics" % "metrics-core" % "2.0.+",
  "com.yammer.metrics" %% "metrics-scala" % "2.0.+"
)

scalaVersion := "2.9.1"

fork in run := true

javaOptions in run += "-Xmx1024m"
