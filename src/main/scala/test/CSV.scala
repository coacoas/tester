package test
import com.yammer.metrics.scala.Timer
import java.util.concurrent.TimeUnit

trait CSV[-A] {
  def csv(a: A): List[(String,String)]
}

object CSVs { 
  def csv[A](a: A)(implicit sh: CSV[A]): List[(String, String)] = sh.csv(a)
  
  def toCSV[A](f: A => List[(String,String)]) = new CSV[A] { 
    override def csv(a: A) = f(a)
  }
  
  implicit val timerCSV: CSV[Timer] = toCSV { timer =>
    import Shows._
    val snapshot = timer.snapshot
    val durationUnit = show(timer.durationUnit)
    val rateUnit = "%s/%s".format(timer.eventType, show(timer.rateUnit))
    val headers = List("#",
        "mean rate (%s)".format(rateUnit),
        "1-minute rate (%s)".format(rateUnit), 
        "5-minute rate(%s)".format(rateUnit), 
        "15-minute rate(%s)".format(rateUnit), 
        "min (%s)".format(durationUnit), 
        "max (%s)".format(durationUnit), 
        "mean (%s)".format(durationUnit), 
        "stddev (%s)".format(durationUnit), 
        "median (%s)".format(durationUnit), 
        "75%% (%s)".format(durationUnit), 
        "95%% (%s)".format(durationUnit), 
        "98%% (%s)".format(durationUnit), 
        "99%% (%s)".format(durationUnit), 
        "99.9%% (%s)".format(durationUnit))
    val values = List(
        "%d".format(timer.count),
        "%.2f".format(timer.meanRate),
        "%.2f".format(timer.oneMinuteRate),
        "%.2f".format(timer.fiveMinuteRate),
        "%.2f".format(timer.fifteenMinuteRate),
        "%.2f".format(timer.min),
        "%.2f".format(timer.max),
        "%.2f".format(timer.mean),
        "%.2f".format(timer.stdDev),
        "%.2f".format(snapshot.getMedian()),
        "%.2f".format(snapshot.get75thPercentile()),
        "%.2f".format(snapshot.get95thPercentile()),
        "%.2f".format(snapshot.get98thPercentile()),
        "%.2f".format(snapshot.get99thPercentile()),
        "%.2f".format(snapshot.get999thPercentile()))
    headers.zip(values)
  }
}