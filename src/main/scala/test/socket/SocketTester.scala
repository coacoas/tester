package test.socket

import test._
import Support._
import java.net.{ ServerSocket, Socket }
import java.io._
import resource._
import scala.io.Source
import java.util.concurrent.TimeUnit
import com.yammer.metrics.scala.Instrumented

case class StringResult(data: Option[String])

trait KeepAliveTestCase extends TestCase[StringResult] with CleanupTestCase[StringResult] {
  import Support._
  
  val socket: Socket
  val data: Seq[String]
  lazy val dataIterator = new CircularIterator(data)
      
  override def test: StringResult = StringResult(Option(Support.requestResponse(
      socket.getOutputStream,
      socket.getInputStream, 
      dataIterator.next + "\r\n")))
  override def validate(result: StringResult) = result.data.isDefined
  
  override def cleanup = socket.close
}

abstract trait CleanupTestCase[T] extends TestCase[T] {
  def cleanup: Unit
  abstract override def stop(iteration: Int) = super.stop(iteration) ~ { done => if (done) cleanup}
}

class KeepAliveTimedTestGenerator(val host: String, val port: Int, val _data: Seq[String], val _duration: Int, val _unit: TimeUnit)
  extends TestGenerator[KeepAliveTestCase] with Instrumented {
  override def newTest: KeepAliveTestCase = new TimedTestCase[StringResult](_duration, _unit) 
    with KeepAliveTestCase {
      override val socket = new Socket(host, port)
      override val data = _data
    }
}

class KeepAliveMessageCountGenerator(host: String, port: Int, _data: Seq[String], _count: Int) 
extends TestGenerator[KeepAliveTestCase] with Instrumented { 
  override def newTest: KeepAliveTestCase = new IterationTestCase[StringResult](_count) with KeepAliveTestCase {
    override val socket = new Socket(host, port)
    override val data = _data
  }
}

object DirectoryTester {
   def filesToStringList(dir: File) = dir.listFiles.toSeq.map(f => Source.fromFile(f).mkString)
}

object EchoTester extends App with Arguments { 
  import DirectoryTester._
  
  val threads = nextArg.getOrElse("1").toInt
  val duration = nextArg.getOrElse("10").toInt
  val host = nextArg.getOrElse("localhost")
  val port = nextArg.getOrElse("8007").toInt
  val data = filesToStringList(new File(nextArg.getOrElse(".")))
  
  new Tester(threads, new KeepAliveTimedTestGenerator(host, port, data, duration, TimeUnit.SECONDS)) with StandardProcessor {
    override val numThreads = threads
  }.call
}

object EchoSuite extends App with Arguments with Suite { 
  import DirectoryTester._

  val threads = List(1, 20, 40, 60, 80)
  val host = nextArg.getOrElse("localhost")
  val port = nextArg.getOrElse("8007").toInt
  val data = filesToStringList(new File(nextArg.getOrElse(".")))
  val duration = nextArg.getOrElse("300").toInt
  val unit = TimeUnit.SECONDS
  
  threads.map(threadCount => 
    new Tester(threadCount, new KeepAliveTimedTestGenerator(host, port, data, duration, unit)) with StandardProcessor {
      override val numThreads = threadCount
    }.call) ~ writeToFile ~ (l => l.map(println))
}