package test
import com.yammer.metrics.stats.Snapshot

trait Show[-A] {
  def show(a: A): String
}

trait LowPriorityShows {
  implicit val AnyShow = new Show[Any] { def show(a: Any) = a.toString }
}

object Shows {
  def show[A](a: A)(implicit sh: Show[A]) = sh.show(a)

  import java.util.concurrent.TimeUnit
  import com.yammer.metrics.scala._
  
  implicit val DurationUnitShow: Show[TimeUnit] = new Show[TimeUnit] {
    import TimeUnit._
    def show(unit: TimeUnit) = unit match {
      case NANOSECONDS  => "ns"
      case MICROSECONDS => "us"
      case MILLISECONDS => "ms"
      case SECONDS      => "s"
      case MINUTES      => "m"
      case HOURS        => "h"
      case DAYS         => "d"
      case _ =>
        throw new IllegalArgumentException("Unrecognized TimeUnit: " + unit);
    }
  }
  
  implicit val TimerShow: Show[Timer] = new Show[Timer] {
    def show(timer: Timer) = {
      val durationUnit = implicitly[Show[TimeUnit]].show(timer.durationUnit)
      val rateUnit = implicitly[Show[TimeUnit]].show(timer.rateUnit)

      val snapshot = timer.snapshot
      "             count = %d\n".format(timer.count) +
        "         mean rate = %2.2f %s/%s\n".format(timer.meanRate, timer.eventType, rateUnit) +
        "     1-minute rate = %2.2f %s/%s\n".format(timer.oneMinuteRate, timer.eventType, rateUnit) +
        "     5-minute rate = %2.2f %s/%s\n".format(timer.fiveMinuteRate, timer.eventType, rateUnit) +
        "    15-minute rate = %2.2f %s/%s\n".format(timer.fifteenMinuteRate, timer.eventType, rateUnit) +
        "               min = %2.2f %s\n".format(timer.min, durationUnit) +
        "               max = %2.2f %s\n".format(timer.max, durationUnit) +
        "              mean = %2.2f %s\n".format(timer.mean, durationUnit) +
        "            stddev = %2.2f %s\n".format(timer.stdDev, durationUnit) +
        "            median = %2.2f %s\n".format(snapshot.getMedian(), durationUnit) +
        "              75%% <= %2.2f %s\n".format(snapshot.get75thPercentile(), durationUnit) +
        "              95%% <= %2.2f %s\n".format(snapshot.get95thPercentile(), durationUnit) +
        "              98%% <= %2.2f %s\n".format(snapshot.get98thPercentile(), durationUnit) +
        "              99%% <= %2.2f %s\n".format(snapshot.get99thPercentile(), durationUnit) +
        "            99.9%% <= %2.2f %s\n".format(snapshot.get999thPercentile(), durationUnit)
    }
  }
}