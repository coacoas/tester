import java.net.{URL, URLEncoder, HttpURLConnection}
import java.io.{PrintWriter, InputStream}
import java.util.concurrent.{TimeUnit, Executors, Callable}
import java.util.Date
import java.text.SimpleDateFormat

import com.yammer.metrics.reporting._
import com.yammer.metrics.Metrics
import com.yammer.metrics.scala._

import scala.io.Source
import scala.annotation.tailrec

import test._
import Support._

case class PostResult(code: Int, msg: String, stream: Option[String])

trait PostTest extends TestCase[PostResult] {
  val url: String
  val data: String
  
  def test = {
    val fullURL = "%s?request=%s".format(url, URLEncoder.encode(data, "UTF-8"))
    val con = new URL(fullURL).openConnection.asInstanceOf[HttpURLConnection]
    con.setRequestMethod("GET")
    con.setDoInput(true)
    con.setDoOutput(true)
    val response = Source.fromInputStream(con.getInputStream).getLines.mkString

    PostResult(con.getResponseCode, con.getResponseMessage, Some(response))
  }

  override def validate(result: PostResult) =
    (result.code == 200) ~ { valid =>
      if (!valid) {
        println("Recieved: %d".format(result.code))
        println("Message: %s".format(result.msg))
      }
    }
}

class TestByMessageCount(val url: String, val data: String, val messageCount: Int, val name: String)
  extends TestGenerator[PostTest] with Instrumented {
  self =>
  lazy val timer: Timer = metrics.timer(name)
  def newTest = new IterationTestCase[PostResult](messageCount) with PostTest with MeteredTestCase[PostResult] {
    override val url = self.url
    override val data = self.data
    override val timer = self.timer
  }
}

class TestByTime(val url: String, val data: String, val duration: Int, val unit: TimeUnit, val name: String)
  extends TestGenerator[PostTest] with Instrumented {
  self =>
  lazy val timer = metrics.timer(name)
  def newTest = new TimedTestCase[PostResult](duration, unit) with PostTest with MeteredTestCase[PostResult] {
    override val url = self.url
    override val data = self.data
    override val timer = self.timer
  }
}

class TesterMain(val fileName: String) extends App with Arguments {
  val numThreads = nextArg.getOrElse("1").toInt
  val numMessages = nextArg.getOrElse("3").toInt
  val url = nextArg.getOrElse("http://prodvcbgate1:8080/rxhub")

  val countCase = new TestByMessageCount(url, Source.fromFile(fileName).mkString, numMessages, 
      "%s [%d threads]".format(fileName, numThreads))
  
  println("%s\n%s".format(fileName, new Tester(numThreads, countCase) with MetricsProcessor {
    val timer = countCase.timer
  }.call))
}

class TesterTime(val fileName: String) extends App with Arguments {
  val numThreads = nextArg.getOrElse("1").toInt
  val seconds = nextArg.getOrElse("10").toInt
  val url = nextArg.getOrElse("http://prodvcbgate1:8080/rxhub")
  
  val timeCase = new TestByTime(url, Source.fromFile(fileName).mkString, seconds, TimeUnit.SECONDS, 
      "%s [%d threads]".format(fileName, numThreads))
  new Tester(numThreads, timeCase) with MetricsProcessor {
    val timer = timeCase.timer
  }.call
}

object EDITester extends TesterMain("270_5010.in")
object EDITime extends TesterTime("270_5010.in")

object XMLTester extends TesterMain("RxHist_Valid.in")
object XMLTime extends TesterTime("RxHist_Valid.in")

class PostSuite(time: Int, unit: TimeUnit) extends Suite {

  val suite = List(1, 2, 4, 6, 8, 15 /*, 25, 100, 200 */ ).map(i => (i, time))
  val fileNames = List("270_5010.in", "RxHist_Valid.in")
  val url = "http://prodvcbgate1:8080/rxhub"

  def results = (for {
    file <- fileNames
    (threadCount, time) <- suite
  } yield {
    val generator = new TestByTime(url, Source.fromFile(file).mkString, time, unit,
      "%s [%d threads]".format(file, threadCount))
    new Tester(threadCount, generator) with MetricsProcessor {
      override lazy val timer = generator.timer
      override def processResults(results: List[TestResult], elapsed: Long) = {
        val result = super.processResults(results, elapsed)
        ("File name", file) :: ("Threads", threadCount.toString) :: result
      }
    }
  }).map(_.call)

  def run = results ~ writeToFile
}

object SuiteApp extends App with Arguments {
  val time = nextArg.getOrElse("10").toInt
  val loops = nextArg.getOrElse("1").toInt

  @tailrec def runLoops(left: Int): Unit = if (left > 0) {
	new PostSuite(time, TimeUnit.SECONDS).run
    runLoops(left - 1)
  }

  runLoops(loops)
}
