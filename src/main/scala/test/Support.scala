package test

import java.util.Date
import java.io.InputStreamReader

object Support {
  import java.io.{ OutputStream, InputStream, PrintWriter }
  import io.Source

  class NotImplementedYetException extends Exception

  /**
   * Returns a tuple with the time elapsed for calling the parameter f (in ms)
   * and the return value of f.
   *
   * @param f
   * @return
   */
  def time[T](f: => T): (Long, T) = {
    val start = new Date().getTime
    val result = f
    val end = new Date().getTime
    (end - start, result)
  }

  implicit def fileToPrintWriter(f: java.io.File): java.io.PrintWriter = {
    if (!f.getParentFile().exists()) {
      f.getParentFile().mkdirs()
    }
    new java.io.PrintWriter(f)
  }

  def printToFile(p: java.io.PrintWriter)(op: java.io.PrintWriter => Unit) {
    try { op(p) } finally { p.close() }
  }
  
  implicit def stringToFile(s: String): java.io.File = new java.io.File(s)

  def ??? = throw new NotImplementedYetException

  implicit def anyWithSideEffects[T](any: T) = new {
    def ~(fn: T => Unit) = {
      fn(any)
      any
    }
  }

  def writeToStream(stream: OutputStream, data: String) {
    stream.write(data.getBytes)
    stream.flush
  }

  def readFromStream(stream: InputStream, eod: String): String = {
    val reader = new InputStreamReader(stream)
    val buffer = new StringBuilder
    while (!buffer.endsWith(eod)) {
      buffer.append(reader.read.toChar)
    }

    buffer.toString
  }

  def requestResponse(request: OutputStream, response: InputStream, data: String, eod: String = "\r\n"): String = {
    writeToStream(request, data)
    readFromStream(response, eod)
  }
}