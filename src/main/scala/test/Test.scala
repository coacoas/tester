package test;

import java.util.Date
import java.text.{ SimpleDateFormat, DateFormat }
import java.util.concurrent._
import scala.math
import scala.annotation.tailrec
import java.io.InputStreamReader
import com.yammer.metrics.scala.Instrumented
import com.yammer.metrics.reporting.ConsoleReporter
import com.yammer.metrics.scala.Timer
import com.yammer.metrics.stats.Snapshot
import Support._

case class TestResult(elapsed: Long, valid: Boolean)
abstract class TestCase[T] extends Callable[List[TestResult]] {

  def test: T
  def validate(result: T): Boolean
  def stop(iter: Int): Boolean = false

  def call = {
    @tailrec
    def callRecursive(acc: List[TestResult], iter: Int)(f: () => TestResult): List[TestResult] =
      if (stop(iter)) acc
      else callRecursive(f() :: acc, iter + 1)(f)

    val results = callRecursive(List[TestResult](), 0)(() => {
      val (elapsed, result) = Support.time(test)
      TestResult(elapsed, validate(result))
    })
    results
  }
}

trait MeteredTestCase[T] extends TestCase[T] {
  val timer: Timer
  lazy val snapshot = timer.snapshot // Initialized after the test is complete

  abstract override def test = timer.time(super.test)
}

abstract class IterationTestCase[A](val count: Int) extends TestCase[A] {
  override def stop(iter: Int) = iter >= count
}

abstract class TimedTestCase[A](val duration: Int, val unit: TimeUnit) extends TestCase[A] {
  var complete = false
  new Thread {
    override def run = {
      Thread.sleep(TimeUnit.MILLISECONDS.convert(duration, unit))
      complete = true
    }
  }.start

  override def stop(iter: Int) = complete
}

class CircularIterator[T](val s: Seq[T]) extends Iterator[T] {
  var linearIterator = s.toIterator

  def hasNext = true
  def next: T = {
    linearIterator = if (linearIterator.hasNext) linearIterator else s.toIterator
    linearIterator.next
  }
}

trait Arguments {
  this: App =>
  lazy val argsIterator = args.toIterator
  def nextArg: Option[String] = try { Option(argsIterator.next) } catch { case _ => None.asInstanceOf[Option[String]] }
  def getArg(arg: => String): Option[String] = try { Option(arg) } catch { case _ => None.asInstanceOf[Option[String]] }
}

trait TestGenerator[+T <: TestCase[_]] {
  def newTest: T
}

case class TestStatistics(
  numThreads: Int,
  numMessages: Int,
  elapsedTime: Long,
  min: Double,
  max: Double,
  stddev: Double,
  mean: Double,
  ninetyPercent: Double) {

  def toList: List[(String, String)] = List("threads", "messages", "elapsed", "min", "max", "stddev", "mean", "90%").zip(
    List(numThreads, numMessages, elapsedTime, min, max, stddev, mean, ninetyPercent).map("%.3f".format(_)))

  override def toString =
    "%10s | %10s | %10s | %10s | %10s | %10s | %10s | %10s".
      format("threads", "messages", "elapsed", "min", "max", "stddev", "mean", "90%\n") +
      "%10d | %10d | %10d | %10.3f | %10.3f | %10.3f | %10.3f | %10.3f\n".
      format(numThreads, numMessages, elapsedTime, min, max, stddev, mean, ninetyPercent)
}

abstract class Tester[T <: TestCase[_]](val numThreads: Int, val testCase: TestGenerator[T], val name: String)
  extends Callable[List[(String, String)]] with Instrumented {

  def this(numThreads: Int, testCase: TestGenerator[T]) {
    this(numThreads, testCase, "Test [%d Threads]".format(numThreads))
  }

  def processResults(results: List[TestResult], elapsed: Long): List[(String, String)]

  def call: List[(String, String)] = {
    val startTime = new Date()
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    println("Starting at %s (%d threads)".format(sdf.format(startTime), numThreads))

    val (elapsed, results) = time {
      val executors = Executors.newFixedThreadPool(numThreads)
      val results = (1 to numThreads).map(i => executors.submit(testCase.newTest))
      executors.shutdown
      results.toList.map(_.get)
    }

    val endTime = new Date()
    println("\nStart time: %s\nEnd time: %s\nTotal time: %d ms".format(sdf.format(startTime), sdf.format(endTime), elapsed))
    processResults(results.flatten, elapsed) ~ { xs =>
      xs.map {
        case (header, value) => println("%s: %s".format(header, value))
      }
    }
  }
}

trait MetricsProcessor {
  val timer: Timer

  def processResults(results: List[TestResult], elapsed: Long) = {
    import CSVs._
    ("elapsed (ms)", elapsed.toString) :: csv(timer)
  }
}

trait StandardProcessor {
  val numThreads: Int

  def stdDev(s: List[Long]): Double = math.sqrt {
    val size = s.size
    if (size >= 2) {
      val mean = (s.sum.toDouble / size)
      val factor = 1.0 / (size.toDouble - 1)
      factor * s.foldLeft(0.0) { (acc, x) => acc + math.pow(x - mean, 2) }
    } else {
      0.0
    }
  }

  def processResults(results: List[TestResult], elapsed: Long) = {
    val times = results.map(_.elapsed)
    val numMessages = results.size
    val min = times.min / 1000.0
    val max = times.max / 1000.0
    val stddev = stdDev(times) / 1000.0
    val mean = (times.sum.toDouble / 1000.0) / numMessages
    val ninetyPercent = (times.sorted.take(math.round((numMessages.toFloat * 0.9).toFloat)).max) / 1000.0

    TestStatistics(numThreads, numMessages, elapsed, min, max, stddev, mean, ninetyPercent).toList
  }

}

trait Suite {
  import Support._

  val dayFormat = new SimpleDateFormat("yyyyMMdd")
  implicit val sdf = new SimpleDateFormat("yyyyMMddHHmmss.SSS")
  private def timestamp(time: Date = new Date())(implicit sdf: SimpleDateFormat) = sdf.format(time)

  def writeToFile(results: List[List[(String, String)]])(implicit logFile: java.io.File = new java.io.File("results/%s/suite%s.csv".
    format(dayFormat.format(new Date()), timestamp()))) =
    printToFile(logFile) { p =>
      p.println(results.head.map(_._1).mkString(","))
      for (result <- results) {
        p.println(result.map(_._2).mkString(","))
      }
    }
}

